package com.paic.arch.interviews.support;

import java.text.ParseException;

/**
 * 时间转换工具
 * 获取具体的时分秒值
 */
public interface TimeTool {
	
	/*
	 * 获取时间中的秒值
	 */
	int getSecond(String time) throws ParseException;
	
	/*
	 * 获取时间中的分值
	 */
	int getMinute(String time) throws ParseException;
	
	/*
	 * 获取时间中的时值
	 */
	int getHour(String time) throws ParseException;
	
	/*
	 * 校验时间参数的合法性
	 * 1、小时数在25以下
	 * 2、分钟数小于60
	 * 3、秒数在60以下
	 * 4、不可超过 24:00:00
	 */
	void validate(String time) throws ParseException;
	
	
	

}
