package com.paic.arch.interviews.support;

import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TheoryClock服务，根据传入时间参数，产生理论时钟的返回字符串
 * 
 */
public class TheoryClockServiceImpl implements TheoryClockService {

	private static Logger log = LoggerFactory.getLogger(TheoryClockServiceImpl.class);
	
	/*
	 * 时钟第二行小时数为0情况
	 */
	private final static  String THEORY_CLOCK_HOUR_SUM_ZERO = "OOOO";
	
	/*
	 * 时钟第二行小时数为20情况
	 */
	private final static  String THEORY_CLOCK_HOUR_SUM_MAX = "RRRR";
	
	/*
	 * 时钟第三行小时数为0情况
	 */
	private final static  String THEORY_CLOCK_HOUR_ZERO = "OOOO"; 
	
	/*
	 * 时钟第三行小时数为最大情况
	 */
	private final static  String THEORY_CLOCK_HOUR_MAX = "RRRR"; 
	
	/*
	 * 分钟第四行分钟数为0情况
	 */
	private final static  String THEORY_CLOCK_MIN_SUM_ZERO = "OOOOOOOOOOO";
	
	/*
	 * 分钟第四行分钟数为最大情况
	 */
	private final static  String THEORY_CLOCK_MIN_SUM_MAX = "YYRYYRYYRYY";
	
	/*
	 * 分钟第五行分钟数为0情况
	 */
	private final static  String THEORY_CLOCK_MIN_ZERO = "OOOO";
	
	/*
	 * 分钟第五行分钟数为最大情况
	 */
	private final static  String THEORY_CLOCK_MIN_MAX = "YYYY";
	
	@Override
	public String getTheoryClock(String time) throws ParseException {
		
		log.debug("TheoryClock start time is " + time);
		
		StringBuffer theroyClockString = new StringBuffer();
		
		TimeTool timeTool = new TimeToolImpl();
		try {
			
			timeTool.validate(time);
			
			int sec = timeTool.getSecond(time);
			
			int min = timeTool.getMinute(time);
			
			int hour = timeTool.getHour(time);
			
			int secLeft =sec % 2;
			
			/*
			 * 处理第一行灯
			 */
			if (secLeft == 0) {
				
				theroyClockString.append("Y");
			} else {
				
				theroyClockString.append("O");
			}
			
			int hourSum = hour/5;
			setTheoryClockLine(theroyClockString, hourSum, THEORY_CLOCK_HOUR_SUM_MAX, THEORY_CLOCK_HOUR_SUM_ZERO);
			
			int hourLeft = hour%5;
			setTheoryClockLine(theroyClockString, hourLeft, THEORY_CLOCK_HOUR_MAX, THEORY_CLOCK_HOUR_ZERO);
			
			int minSUM = min/5;
			setTheoryClockLine(theroyClockString, minSUM, THEORY_CLOCK_MIN_SUM_MAX, THEORY_CLOCK_MIN_SUM_ZERO);
			
			int minLeft = min%5;
			setTheoryClockLine(theroyClockString, minLeft, THEORY_CLOCK_MIN_MAX, THEORY_CLOCK_MIN_ZERO);
			
		} catch (ParseException e) {
			
			log.error(e.getMessage(), e);
			throw e;
		}
		
		
		
		return theroyClockString.toString();
	}

	private void setTheoryClockLine(StringBuffer theroyClockString, int hourSum, String maxString, String zeroString) {
		theroyClockString.append("\r\n");	
		theroyClockString.append(maxString.substring(0, hourSum))
		.append(zeroString.substring(hourSum, zeroString.length()));
	}
	
	public static void main(String[] args) {
		System.out.println(15%5);
		String a = "RRRR";
		String b = "OOOO";
		StringBuffer c =new StringBuffer();
		c.append(a.substring(0, 0)).append(b.substring(0, b.length()));
//		System.out.println(a.substring(1, a.length()));
		System.out.println(c);
		TheoryClockService theoryClockService = new TheoryClockServiceImpl();
		try {
			theoryClockService.getTheoryClock("21:80:11");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
