package com.paic.arch.interviews.support;

import java.text.ParseException;

public interface TheoryClockService {
	
	String getTheoryClock(String time) throws ParseException;

}
