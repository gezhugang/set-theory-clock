package com.paic.arch.interviews;

import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.paic.arch.interviews.support.TheoryClockService;
import com.paic.arch.interviews.support.TheoryClockServiceImpl;

public class TimeConverterImpl implements TimeConverter {
	
	Logger log = LoggerFactory.getLogger(TimeConverterImpl.class);

	@Override
	public String convertTime(String time) {
		
		TheoryClockService theoryClockService =new TheoryClockServiceImpl();
		
		try {
			return theoryClockService.getTheoryClock(time);
		} catch (ParseException e) {
			e.printStackTrace();
			log.error(e.getMessage(), e);
		}
		return "";
		
	}
	
	public static void main(String[] args) {
		TimeConverter timeConverter = new TimeConverterImpl();
		System.out.println(timeConverter.convertTime("23:59:59"));
	}

}
